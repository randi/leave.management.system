﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(leave.management.system.web.Startup))]
namespace leave.management.system.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
